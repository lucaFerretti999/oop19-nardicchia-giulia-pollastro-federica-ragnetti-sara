# Brain Trainer #

### Built With ###

Java FX - Graphical User Interface
JUnit - Testing

### Authors ###

*Nardicchia Giulia, Pollastro Federica, Ragnetti Sara, Vitali Anna, Yan Elena*

### Instructions ###
Download and run brainTrainer.jar . Please make sure you have a working JRE installed.

Create a new account or insert username and password to start. Use mouse to switch between the pages of the application. Click on play button and select the mini-game to start playing.
