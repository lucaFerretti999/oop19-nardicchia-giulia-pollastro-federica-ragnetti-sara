package model.minigames.oneway;

import model.minigames.MiniGameSettings;

/**
 * The interface to change One Way settings.
 *
 */
public interface OneWaySettings extends MiniGameSettings {

    /**
     * 
     * @return the arrows count
     */
    int getArrows();
}
