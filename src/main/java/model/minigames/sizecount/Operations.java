package model.minigames.sizecount;

public enum Operations {

    /**
     * Make an addiction.
     */
    ADDICTION,

    /**
     * Make a subtraction.
     */
    SUBTRACTION,

    /**
     * Make a division.
     */
    DIVISION,

    /**
     * Make a multiplication.
     */
    MULTIPLICATION,

    /**
     * Make an addiction expression.
     */
    SUM_EXPRESSION,

    /**
     * Make a subtraction expression.
     */
    SUB_EXPRESSION,

}
