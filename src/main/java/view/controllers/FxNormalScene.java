package view.controllers;

/** 
 * This interface represents the common parts of the normal scene.
 *
 */
public interface FxNormalScene extends FxController {

    /**
     * Initialize the scene.
     */
   void init();
}
